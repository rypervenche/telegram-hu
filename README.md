# Hungarian translation for Telegram

## Android

The original XML file was found on http://toldyzoltan.hu/android-magyaritasok/

I have since merged it with a current version of the English strings.xml file to bring it up-to-date.

There are still many translations that need to be added, however I likely only have the skill to update some of the easier ones, as I am not a native speaker.

## Desktop

I have slowly been adding translations to the Desktop Telegram version, however since I'm not starting ontop of someone else's work, only very apparent translations will likely get done. I will try to add as much as I can until Telegram finally allows us to add our own languages and translations, which is coming "soon".
